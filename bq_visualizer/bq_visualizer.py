import random
from logging import getLogger
from pprint import pprint
import pandas as pd
import numpy as np
import bqplot as bq
import ipywidgets as ipyw
import traitlets as trt

logger = getLogger(__name__)

class DataPlotting(ipyw.VBox):
    '''
    In order to view output (text form), will need to uncomment 'pprint(_)'...
    (right below _populate_graphs)..commented out while working on testing
    '''

    column_list = trt.List()
    select_columns = trt.Instance(ipyw.SelectMultiple)
    selected_columns = trt.List()
    select_plot_type = trt.Instance(ipyw.Select)
    selected_plot_type = trt.Unicode()
    options_dict = trt.Dict()

    figs_per_row = trt.Instance(ipyw.IntSlider)
    selected_num_figs = trt.Int()

    figures = trt.List(trt.Instance(bq.figure.Figure))

    separated = trt.Bool()
    separate_check = trt.Instance(ipyw.Checkbox)
    color_column = trt.Instance(ipyw.Select)
    selected_color_column = trt.Any()

    dataframe = trt.Instance(pd.core.frame.DataFrame)
    plot_output = trt.Instance(ipyw.HBox)
    output = ipyw.Output()

    type_to_function_dictionary = {
        "Histogram": "histograms",
        "Line Graph": "line_graph",
        "Multiple Histograms": "histograms",
        "Multiple Line Graph": "line_graph",
        "Scatter Plot": "scatter_plot",
        "Color Scatter Plot": "color_scatter_plot",
    }

    options_dict = {
        1: ["Histogram", "Line Graph",],
        2: ["Multiple Histograms", "Multiple Line Graph", "Scatter Plot",],
        3: ["Multiple Histograms", "Multiple Line Graph", "Color Scatter Plot",],
        4: ["Multiple Histograms", "Multiple Line Graph",],
    }

    @trt.default("separate_check")
    def _make_check(self):
        check = ipyw.Checkbox(
            value=False, disabled=False, description="Separate line graphs?"
        )

        trt.link((check, "value"), (self, "separated"))

        return check

    @trt.default("figs_per_row")
    def _make_figs_selector(self):
        style = {"description_width": "initial"}
        num_select = ipyw.IntSlider(
            min=1,
            max=4,
            disabled=False,
            value=1,
            description="Figures Per Row:",
            style=style,
        )

        trt.link((num_select, "value"), (self, "selected_num_figs"))
        return num_select

    @trt.default("color_column")
    def _make_color_column(self):
        color_columns = ipyw.Select(options=self.selected_columns, disabled=False,)
        trt.link((color_columns, "value"), (self, "selected_color_column"))
        return color_columns

    # default graph marks/figures

    @trt.default("plot_output")
    def _make_plot_output(self):
        return ipyw.HBox(layout=ipyw.Layout(flex_flow="row wrap"))

    # list of selectable columns (must be numeric)
    @trt.default("column_list")
    def _make_column_list(self):
        return list(self.dataframe.select_dtypes("number").columns)

    @trt.default("select_columns")
    def _make_select_columns(self):
        selector = ipyw.SelectMultiple(options=self.column_list, disabled=False,)
        trt.link((selector, "value"), (self, "selected_columns"))
        return selector

    @trt.default("select_plot_type")
    def _make_output_widget(self):
        plot_selector = ipyw.Select(
            options=["Please select one or more columns"], disabled=False
        )

        trt.link((plot_selector, "value"), (self, "selected_plot_type"))
        return plot_selector

    # observe methods
    @trt.observe("selected_columns")
    def _update_output_widget(self, *_):
        if len(self.selected_columns) == 1:
            self.select_plot_type.options = self.options_dict[1]
        elif len(self.selected_columns) == 2:
            self.select_plot_type.options = self.options_dict[2]
        elif len(self.selected_columns) == 3:
            self.select_plot_type.options = self.options_dict[3]
        elif len(self.selected_columns) > 3:
            self.select_plot_type.options = self.options_dict[4]
        else:
            self.select_plot_type.options = ["Please select one or more columns"]
        self.color_column.options = self.selected_columns

    @trt.observe("dataframe")
    def _update_column_list(self, *_):
        self.column_list = self._make_column_list()

    @trt.observe("column_list")
    def _update_select_columns(self, *_):
        self.select_columns.options = self.column_list

    @trt.observe(
        "selected_plot_type",
        "selected_columns",
        "separated",
        "selected_num_figs",
        "selected_color_column",
    )
    @output.capture()
    def _populate_graphs(self, *_):
        # print('registered change, values are:')
        # print('selected_columns',self.selected_columns)
        # print('selected_plot_type',self.selected_plot_type)
        # print('separated?',self.separated)
        #pprint(_)
        if self.selected_plot_type not in self.type_to_function_dictionary:
            logger.debug(
                f"Cannot find {self.selected_plot_type} in {self.type_to_function_dictionary}"
            )
            return
        method_to_call = getattr(
            self, self.type_to_function_dictionary[self.selected_plot_type], None
        )
        if method_to_call is None:
            selected_method = self.type_to_function_dictionary[self.selected_plot_type]
            logger.debug(f"Couldn't find method {selected_method} in class")
            return

        self.update_ui()
        method_to_call()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dataframe = kwargs["dataframe"]

        self.title = ipyw.HTML("<h1>Data Analysis Tool</h1>")

        self.instructions_1 = ipyw.HTML("Select columns of dataframe to be plotted:")

        self.instructions_2 = ipyw.HTML("Select plot type and number of plots per row:")

        self.color_instructions = ipyw.HTML("Select column to be color column:")

        self.colors = list(np.random.choice(range(256), size=20))

        self.update_ui()

    # write methods for creating the actual graphs
    def get_data(self):
        """Pull the data from the dataframe based on columns selected"""
        try:
            return {
                column_name: self.dataframe[column_name].tolist()
                for column_name in self.selected_columns
            }
        except KeyError:
            missing_columns = set(self.selected_columns) - set(
                self.dataframe.columns
            )
            logger.debug(f"Cannot find column {missing_columns} in dataframe")
            return

    def histograms(self):
        """Create histogram plots"""
        plot_data = self.get_data()
        num_figures = len(plot_data)
        self.edit_figures(num_figures)

        for figure, (column, data) in zip(self.figures, plot_data.items()):
            hist, axis = self._make_hist_marks_axes(data, column)

            figure.marks = hist
            figure.title = "Histogram of {}".format(column)
            figure.axes = axis
            figure.layout = {"width": "{}%".format(100 // self.selected_num_figs - 5)}

        self.plot_output.children = self.figures
        self.update_ui()

    def line_graph(self):
        """Called when line graph selected, then redirected to seperate function"""
        if not self.separated:
            self.single_line_graph()
        else:
            self.separated_line_graphs()

    def scatter_plot(self):
        """Method to set up scatter plots"""
        plot_data = self.get_data()
        num_figures = 1
        self.edit_figures(num_figures)

        try:
            x_variable = self.selected_columns[1]
            y_variable = self.selected_columns[0]
            x_data = plot_data[x_variable]
            y_data = plot_data[y_variable]
        except IndexError:
            logger.debug("Need to have two columns selected")
            return

        scatter, axis = self._make_scatter_marks_axes(
            x_variable, x_data, y_variable, y_data, None, None
        )

        self.figures[0].marks = scatter
        self.figures[0].title = "Scatter Plot of {} vs {}".format(
            y_variable, x_variable
        )
        self.figures[0].axes = axis[:2]
        self.figures[0].layout = {
            "width": "{}%".format(100 // self.selected_num_figs - 5)
        }
        self.plot_output.children = self.figures

        self.update_ui()

    def single_line_graph(self):
        """Method for building one-figure line graph(as many lines as desired)"""
        plot_data = self.get_data()
        num_figures = 1
        self.edit_figures(num_figures)

        outlier_column = self.find_outlier_column(plot_data)

        y_data = list(plot_data.values())  # set equal to data
        column_names = list(plot_data.keys())

        if outlier_column is None:
            graph_components = self._make_line_graph_marks_axes(y_data, column_names)

        else:
            graph_components = self._make_two_axis_components(plot_data, outlier_column)

        (lines, axes) = graph_components

        self.figures[0].marks = lines
        self.figures[0].title = ", ".join(column_names)
        self.figures[0].axes = axes
        self.figures[0].layout = {
            "width": "{}%".format(100 // self.selected_num_figs - 5)
        }

        self.plot_output.children = self.figures

        self.update_ui()

    def separated_line_graphs(self):
        """Method for creating multiple figures/line graphs based on options selected"""
        plot_data = self.get_data()
        num_figures = len(plot_data)
        self.edit_figures(num_figures)

        for figure, (column, data) in zip(self.figures, plot_data.items()):
            graph_components = self._make_line_graph_marks_axes([data], [column])
            (lines, axes) = graph_components

            figure.marks = lines
            figure.title = "Line Graph of {}".format(column)
            figure.axes = axes
            figure.layout = {"width": "{}%".format(100 // self.selected_num_figs - 5)}

        self.plot_output.children = self.figures
        self.update_ui()

    def color_scatter_plot(self):
        """Method for creating scatter plot with third var, color axis"""
        plot_data = self.get_data()
        num_figures = 1
        self.edit_figures(num_figures)

        scatter_choices = list(plot_data.keys())
        scatter_choices.remove(self.selected_color_column)
        x_variable = scatter_choices[0]
        y_variable = scatter_choices[1]
        color_variable = self.selected_color_column

        color_scatter, axis = self._make_scatter_marks_axes(
            x_variable,
            plot_data[x_variable],
            y_variable,
            plot_data[y_variable],
            color_variable,
            plot_data[color_variable],
        )

        self.figures[0].marks = color_scatter
        self.figures[0].axes = axis
        self.figures[0].title = "Color Scatter Plot"
        self.figures[0].layout = {
            "width": "{}%".format(100 // self.selected_num_figs - 5)
        }

        self.plot_output.children = self.figures

        self.update_ui()

    def update_ui(self):
        """General Method called whenever the UI needs updated"""
        if self.selected_plot_type == "Color Scatter Plot":
            self.children = (
                self.title,
                self.instructions_1,
                self.select_columns,
                self.instructions_2,
                self.select_plot_type,
                self.separate_check,
                self.figs_per_row,
                self.color_instructions,
                self.color_column,
                self.plot_output,
            )
        else:
            self.children = (
                self.title,
                self.instructions_1,
                self.select_columns,
                self.instructions_2,
                self.select_plot_type,
                self.separate_check,
                self.figs_per_row,
                self.plot_output,
            )

    def edit_figures(self, num_figures: int):
        """Method to delete/add figures until the proper amount reached"""
        if len(self.figures) < num_figures:
            num_new_figures = num_figures - len(self.figures)
            self.figures += [bq.Figure() for _ in range(num_new_figures)]
        else:
            self.figures = self.figures[:num_figures]

    def find_outlier_column(self, plot_data: dict):
        """method to find a column that is significantly greater than
        another column and can then have its own axis. currently defined as
        being 5x bigger than all the max of other columns"""
        if len(plot_data) == 1:
            return None
        maxes = {col_name: max(col_data) for col_name, col_data in plot_data.items()}
        special_column = None
        for column in maxes:
            count = 0
            for other_column in maxes:
                if 0.2 * maxes[column] > maxes[other_column]:
                    count += 1
            if count == len(maxes) - 1:
                special_column = column
        return special_column

    def _make_line_graph_marks_axes(
        self,
        data: list,  # this is actually a list of lists, each nested list being a set of data
        columns: list,
    ) -> tuple:
        """method to make the line marks and axis for a line graph"""
        x_data = list(range(len(self.dataframe.index)))
        scale_x = bq.LinearScale()
        scale_y = bq.LinearScale()
        rand_col = lambda: random.randint(0, 255)
        line_labels = []
        label = ""

        scale_y.max = max([item for sublist in data for item in sublist])
        scale_y.min = min([item for sublist in data for item in sublist])

        if len(columns) > 1:
            label = "Variable Values"
            line_labels = columns

        elif len(columns) == 1:
            label = "{}".format(columns[0])
            line_labels = columns

        lines = bq.Lines(
            x=x_data,
            y=data,
            scales={"x": scale_x, "y": scale_y,},
            labels=line_labels,
            display_legend=True,
            colors=[
                "#%02X%02X%02X" % (rand_col(), rand_col(), rand_col())
                for _ in range(len(columns))
            ],
        )

        x_ax = bq.Axis(scale=scale_x,)

        y_ax = bq.Axis(scale=scale_y, orientation="vertical", label=label)

        return ([lines], [x_ax, y_ax])

    def _make_two_axis_components(self, plot_data: dict, outlier_column: str) -> tuple:
        """Method to make double axis line graph if needed (if there is an outlier)"""
        outlier_data = plot_data[outlier_column]
        plot_data.pop(outlier_column, None)
        non_outliers_data = list(plot_data.values())

        lines1, axis1 = self._make_line_graph_marks_axes(
            non_outliers_data, list(plot_data.keys())
        )

        lines2, axis2 = self._make_line_graph_marks_axes(
            [outlier_data], [outlier_column]
        )

        x_ax = axis1[0]
        y_ax1 = axis1[1]
        y_ax2 = axis2[1]
        y_ax2.side = "right"

        return (lines1 + lines2, [x_ax, y_ax1, y_ax2])

    def _make_scatter_marks_axes(
        self,
        x_label: str,
        x_data: list,
        y_label: str,
        y_data: list,
        color_label: str,
        color_data: list,
    ) -> tuple:
        """Method to make scatter mark and axes (color included)"""

        x_scale = bq.LinearScale()
        y_scale = bq.LinearScale()
        color_scale = bq.ColorScale()

        scatter = bq.Scatter(
            x=x_data,
            y=y_data,
            color=color_data,
            scales={"x": x_scale, "y": y_scale, "color": color_scale,},
        )

        x_ax = bq.Axis(label=x_label, scale=x_scale)

        y_ax = bq.Axis(label=y_label, scale=y_scale, orientation="vertical",)

        color_axis = bq.ColorAxis(
            label="{}".format(color_label), scale=color_scale, side="right"
        )

        return ([scatter], [x_ax, y_ax, color_axis])

    def _make_hist_marks_axes(self, data: list, column: str,) -> tuple:
        """Method to make histogram marks and axes"""
        hist = bq.Hist(
            sample=data,
            normalized=True,
            scales={"sample": bq.LinearScale(), "count": bq.LinearScale(),},
        )

        x_ax = bq.Axis(
            label=column,
            scale=bq.LinearScale(min=float(min(data)), max=float(max(data)),),
        )

        y_ax = bq.Axis(scale=bq.LinearScale(), orientation="vertical",)

        return ([hist], [x_ax, y_ax])
