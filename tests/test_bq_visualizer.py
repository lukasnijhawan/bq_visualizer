import unittest
import pytest
import pandas as pd
import traitlets as trt
import bqplot as bq
from bq_visualizer import DataPlotting

#unittestings
class BasicTestingCases(unittest.TestCase):

    def test_select_columns(self):
        blank_df = pd.DataFrame()
        plotter = DataPlotting(dataframe = blank_df)

        self.assertEqual(len(plotter.column_list),0)

    def test_num_figures_needed(self):
        iris = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv')
        plotter = DataPlotting(dataframe = iris)
        plotter.selected_columns = ['sepal_length','sepal_width']
        plotter.separated = True
        plotter.selected_plot_type = 'Multiple Line Graph'

        self.assertEqual(len(plotter.figures), 2)

    def test_non_numeric_columns(self):
        non_numeric_df = pd.DataFrame({
            'col1':['red','blue','white'],
            'col2':['green','orange','purple'],
            'col3':[1,2,3]
        })
        plotter = DataPlotting(dataframe = non_numeric_df)

        self.assertEqual(len(plotter.column_list), 1)

#coverage library

    def test_line_graph(self):
        iris = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv')
        plotter = DataPlotting(dataframe = iris)
        plotter.separated = False
        plotter.selected_columns = ['sepal_length','sepal_width']
        plotter.selected_plot_type = 'Multiple Line Graph'

        self.assertTrue(type(plotter.figures[0].marks[0]) == bq.Lines)

    def test_hist_graph(self):
        iris = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv')
        plotter = DataPlotting(dataframe = iris)
        plotter.selected_columns = plotter.column_list
        plotter.selected_plot_type = 'Multiple Histograms'

        self.assertTrue(type(plotter.figures[0].marks[0]) == bq.Hist)
        self.assertTrue(len(plotter.figures) == len(plotter.column_list))

    def test_scatter_plot(self):
        iris = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv')
        plotter = DataPlotting(dataframe = iris)
        plotter.selected_columns = ['sepal_length','sepal_width']
        plotter.selected_plot_type = 'Scatter Plot'

        self.assertTrue(type(plotter.figures[0].marks[0]) == bq.Scatter)
        self.assertEqual(len(plotter.figures), 1)

    def test_color_scatter(self):
        iris = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv')
        plotter = DataPlotting(dataframe = iris)
        plotter.selected_columns = plotter.column_list[:3]
        plotter.selected_plot_type = 'Color Scatter Plot'

        self.assertTrue(type(plotter.figures[0].marks[0]) == bq.Scatter)
        self.assertEqual(len(plotter.figures), 1)

    def test_accuracy_of_marks(self):
        iris = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv')
        plotter = DataPlotting(dataframe = iris)

        #histogram
        plotter.selected_columns = ['sepal_length']
        plotter.selected_plot_type = 'Histogram'
        self.assertCountEqual(plotter.figures[0].marks[0].sample, list(iris['sepal_length']))

        #line graph
        plotter.selected_columns = ['sepal_width']
        plotter.selected_plot_type = 'Line Graph'
        self.assertCountEqual(plotter.figures[0].marks[0].y, list(iris['sepal_width']))

        #multiple line graph
        plotter.selected_columns = ['sepal_width','sepal_length']
        plotter.selected_plot_type = 'Multiple Line Graph'
        plotter.separated = True
        self.assertCountEqual(plotter.figures[0].marks[0].y, list(iris['sepal_width']))
        self.assertCountEqual(plotter.figures[1].marks[0].y, list(iris['sepal_length']))

        #scatter plot
        plotter.selected_plot_type = 'Scatter Plot'
        self.assertCountEqual(plotter.figures[0].marks[0].x, list(iris['sepal_length']))
        self.assertCountEqual(plotter.figures[0].marks[0].y, list(iris['sepal_width']))

        #multiple histograms
        plotter.selected_plot_type = 'Multiple Histograms'
        self.assertCountEqual(plotter.figures[1].marks[0].sample, list(iris['sepal_length']))
        self.assertCountEqual(plotter.figures[0].marks[0].sample, list(iris['sepal_width']))

        #color scatter plot
        plotter.selected_columns = ['sepal_width','sepal_length','petal_length']
        plotter.selected_plot_type = 'Color Scatter Plot'
        plotter.selected_color_column = 'petal_length'
        self.assertCountEqual(plotter.figures[0].marks[0].x, list(iris['sepal_width']))
        self.assertCountEqual(plotter.figures[0].marks[0].y, list(iris['sepal_length']))
        self.assertCountEqual(plotter.figures[0].marks[0].color, list(iris['petal_length']))

    def test_multiple_axis(self):
        large_difference_df = pd.DataFrame({
        'col1':[1,2,3,4,5],
        'col2':[2,4,5,3,5],
        'col3':[100,101,102,103,105],
        })
        plotter = DataPlotting(dataframe = large_difference_df)
        plotter.selected_columns = plotter.column_list
        plotter.selected_plot_type = 'Multiple Line Graph'
        plotter.separated = False
        self.assertEqual(len(plotter.figures[0].axes), 3)


#py test tests
def invalid_column_error():
    iris = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv')
    plotter = DataPlotting(dataframe = iris)
    with pytest.raises(TypeError):
        plotter.selected_columns = ['sepal_length','imaginary']
    print('passed invalid column test')


if __name__ == '__main__':
    invalid_column_error()
    unittest.main()
