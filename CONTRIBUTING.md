# Contributing to BQ_Visualizer

## Setting up the environment

### Get Miniconda and Create a 'base' environment
In order to run bq_visualizer you will need certain packages/versions installed. These are best used in a miniconda environment which can be called vis_environment. These can be found in the `environment.yml` file. To run this file run:

```bash
conda env create --name=vis_environment --file=environment.yml
conda activate vis_environment
```

It will then be necessary to install the package in development mode. While in the `bq_visualizer` directory, on the command line type:

```bash
pip install -e .
```

This should put you in the right environment to be able to run all the functionality of the package.

## Running Testing Script
To run the tester, on the command line run:
```bash
pytest
```
This will give a coverage report for the testing file (located in /tests/test_bq_visualizer.py) as well as an accuracy report for the test.

## Adding graphing capabilities to the program
To add a new plot type to the package, the first step is to `type_to_function_dictionary` and the `options_dict` to include the plot you wish to add. The next stop is to create a new method named after the key corresponding to your new value in `type_to_function_dictionary`, which will be called when this option is selected. At the beginning of your new method it will be useful to run `self.get_data()` as well as `self.edit_figures()` in order to configure the data you will use as well as the number of figures you will need.


## Contact
For questions:  
Lukas Nijhawan  
`Lukas.Nijhawan@gtri.gatech.edu`  
