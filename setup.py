from setuptools import setup

setup(
    name="bq_visualizer",
    install_requires = [
    'bqplot==0.12.12',
    'ipywidgets>=7.5,<8',
    'numpy>=1.11,<2',
    'pandas>=1,<2',
    'traitlets>4,<=5',
    ],
    description = 'a package for creating an interactive bqplot display',
    author = 'Georgia Tech Research Institute',
    author_email = 'Lukas.Nijhawan@gtri.gatech.edu',
    python_requires = '>=3.6',
)
