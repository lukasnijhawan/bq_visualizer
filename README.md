# BQ Visualizer

This is a python package that works with a `pandas dataframe` to create an interactive user interface that allows the user to select different types of plots to graph (as of right now: line, histogram, scatter, colored scatter) and immediately see the results of the columns they wish to view. This is done in a JupyterLab or JupyterNotebook setting. This package is designed to be run in the `cortex` setting.


## Setup and run

An example of how the program is implemented (class found in `bq_visualization.py`):

```python
plotter = DataPlotting(dataframe = dataframe_object)
plotter
```

To track errors, it is recommended to run:
```python
plotter.output
```
This will give a detailed explanation of both the plot outputs as well as any errors that pop up. Might need to uncomment the print statement, see class docstring for more information.



=============

To contact:  
Lukas Nijhawan  
Lukas.Nijhawan@gtri.gatech.edu  
715-214-5524
